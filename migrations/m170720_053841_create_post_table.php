<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m170720_053841_create_post_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('post', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'body' => $this->string(),
            'category' => $this->string(),
            'author' => $this->string(),
            'status' => $this->string(),
            'created_at' => $this->string(),
            'updated_at' => $this->string(),
            'created_by' => $this->string(),
            'updated_by' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('post');
    }
}
