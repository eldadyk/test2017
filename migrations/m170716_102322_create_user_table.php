<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170716_102322_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'Name' => $this->string(),
            'UserName' => $this->string(),
            'Password' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
